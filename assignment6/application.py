import os
import time

from flask import request
from flask import Flask, render_template
import mysql.connector
from mysql.connector import errorcode

application = Flask(__name__)
app = application


def get_db_creds():
    db = os.environ.get("DB", None) or os.environ.get("database", None)
    username = os.environ.get("USER", None) or os.environ.get("username", None)
    password = os.environ.get("PASSWORD", None) or os.environ.get("password", None)
    hostname = os.environ.get("HOST", None) or os.environ.get("dbhost", None)
    return db, username, password, hostname


def create_table():
    # Check if table exists or not. Create and populate it only if it does not exist.
    db, username, password, hostname = get_db_creds()
    table_ddl = 'CREATE TABLE movies2(id INT UNSIGNED NOT NULL AUTO_INCREMENT, year INT UNSIGNED NOT NULL, title TEXT NOT NULL, director TEXT NOT NULL, actor TEXT NOT NULL, release_date TEXT NOT NULL, rating FLOAT(4,2) NOT NULL, PRIMARY KEY (id))'
    print(table_ddl)
    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
    except Exception as exp:
        print(exp)

    cur = cnx.cursor()

    try:
        cur.execute(table_ddl)
        cnx.commit()
        populate_data()
    except mysql.connector.Error as err:
        if err.errno == errorcode.ER_TABLE_EXISTS_ERROR:
            print("already exists.")
        else:
            print(err.msg)


def populate_data():

    db, username, password, hostname = get_db_creds()

    print("Inside populate_data")
    print("DB: %s" % db)
    print("Username: %s" % username)
    print("Password: %s" % password)
    print("Hostname: %s" % hostname)

    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                       host=hostname,
                                       database=db)
    except Exception as exp:
        print(exp)

    #cur.execute("INSERT INTO movies2 (year, title, director, actor, release_date, rating) values (1995, 'test_movie', 'edmond', 'amataj', '23 December 1995', 5)")
    #cnx.commit()
    print("Returning from populate_data")


def query_data():

    db, username, password, hostname = get_db_creds()

    print("Inside query_data")
    print("DB: %s" % db)
    print("Username: %s" % username)
    print("Password: %s" % password)
    print("Hostname: %s" % hostname)

    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
    except Exception as exp:
        print(exp)

    cur = cnx.cursor()

    cur.execute("SELECT year, title, director, actor, release_date, rating FROM movies2")
    for (year, title, director, actor, release_date, rating) in cur:
        print("%s, %s, %s, %s, %s, %s" % (year, title, director, actor, release_date, rating))


try:
    print("---------" + time.strftime('%a %H:%M:%S'))
    print("Before create_table")
    create_table()
    print("After create_table")
except Exception as exp:
    print("Got exception %s" % exp)
    conn = None


@app.route('/add_movie', methods=['POST'])
def add_to_db():
    print("Received add request.")
    year = int(request.form['year'])
    title_request = request.form['title'].lower()
    director = request.form['director'].lower()
    actor = request.form['actor'].lower()
    release_date = request.form['release_date'].lower()
    rating = float(request.form['rating'])

    db, username, password, hostname = get_db_creds()

    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
    except Exception as exp:
        print(exp)
        import MySQLdb
        cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)

    cur = cnx.cursor()

    # look for duplicate movie first
    cur.execute("SELECT id, title FROM movies2")
    results = cur.fetchall()
    for (Id, title) in results:
        if title_request == title:
            error_message = "Movie %s could not be inserted - %s" % (title_request, "duplicate entry found")
            return render_template('index.html', messages=[error_message])

    # now add movie
    add_movie_query = "INSERT INTO movies2 (year, title, director, actor, release_date, rating) values (%s, %s, %s, %s, %s, %s)"
    try: 
        cur.execute(add_movie_query, (year, title_request, director, actor, release_date, rating))
        cnx.commit()
        success_message = "Movie %s successfully inserted!" % (title_request)
        return render_template('index.html', messages=[success_message])
    except Exception as e:
        error_message = "Movie %s could not be inserted - <%s>" % (title_request, e)
        return render_template('index.html', messages=[error_message])


@app.route('/delete_movie', methods=['POST'])
def delete_movie():
    print("Received delete request.")
    delete_title = request.form['delete_title'].lower()

    db, username, password, hostname = get_db_creds()

    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
    except Exception as exp:
        print(exp)
        import MySQLdb
        cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)

    cur = cnx.cursor()
    # see if movie exists first
    try:
        cur.execute("SELECT id, title FROM movies2")
        results = cur.fetchall()
        for (Id, title) in results:
            if delete_title == title:
                delete_movie_query = "DELETE FROM movies2 WHERE id = %s"
                cur.execute(delete_movie_query, (Id,))
                cnx.commit()
                success_message = "Movie %s successfully deleted" % (delete_title)
                return render_template('index.html', messages=[success_message])
    except Exception as e:
        print(e)
        error_message = "Movie %s could not be deleted - %s" % (delete_title, e)
        return render_template('index.html', messages=[error_message])
    error_message = "Movie with %s does not exist" % (delete_title)
    return render_template('index.html', messages=[error_message])


@app.route('/update_movie', methods=['POST'])
def update_movie():
    print("Received update request.")
    year = int(request.form['year'])
    title_request = request.form['title'].lower()
    director = request.form['director'].lower()
    actor = request.form['actor'].lower()
    release_date = request.form['release_date'].lower()
    rating = float(request.form['rating'])


    db, username, password, hostname = get_db_creds()

    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
    except Exception as exp:
        print(exp)
        import MySQLdb
        cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)

    cur = cnx.cursor()

    try:
        # first look to see if movie exists in database
        cur.execute("SELECT id, title FROM movies2")
        results = cur.fetchall()
        for (Id, title) in results:
            # update movie if found
            #print("FOUND")
            if title == title_request:
                cur.execute("UPDATE movies2 SET year=%s, director=%s, actor=%s, release_date=%s, rating=%s WHERE id=%s", (year, director, actor, release_date, rating, Id))
                cnx.commit()
                success_message = "Movie %s successfully updated!" % (title_request)
                return render_template('index.html', messages=[success_message])
        return add_to_db()
    except Exception as e:
        print(e)
        error_message = "Movie %s could not be updated - %s" % (title_request, e)
        return render_template('index.html', messages=[error_message])


@app.route('/search_movie', methods=['GET'])
def search_movie():
    print("Received search request.")
    search_actor = request.args['search_actor'].lower()
    db, username, password, hostname = get_db_creds()

    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
    except Exception as exp:
        print(exp)
        import MySQLdb
        cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)

    cur = cnx.cursor()
    search_movie_query = "SELECT year, title, actor FROM movies2"
    cur.execute(search_movie_query)
    results = cur.fetchall()
    movies_with_actor = []
    actors_found = False
    for (year, title, actor) in results:
        if search_actor == actor:
            movies_with_actor.append("%s, %s, %s" % (title, year, actor))
            actors_found = True

    if actors_found == True:
        return render_template('index.html', messages=movies_with_actor)
    else:
        movies_with_actor.append("No movies found for actor %s" % (search_actor))
        return render_template('index.html', messages=movies_with_actor)


@app.route("/highest_rating", methods=['GET'])
def get_highest_rated_movies():
    print("Received highest rated movies request.")
    db, username, password, hostname = get_db_creds()
    max_rating = -1.0
    highest_rated_movies =[]
    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
    except Exception as exp:
        print(exp)
        import MySQLdb
        cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)

    cur = cnx.cursor()

    cur.execute("SELECT title, year, actor, director, rating FROM movies2")
    results = cur.fetchall()
    # first get max rating 
    for title, year, actor, director, rating in results:
        if max_rating < float(rating):
            max_rating = float(rating)

    # now grab all movies with highest rating
    for title, year, actor, director, rating in results:
        if max_rating == float(rating):
            movie = ['%s, %s, %s, %s, %s' % (title, year, actor, director, float(rating))]
            highest_rated_movies.append(movie)

    return render_template('index.html', messages=highest_rated_movies)


@app.route("/lowest_rating", methods=['GET'])
def get_lowest_rated_movies():
    print("Received lowest rated movies request.")
    db, username, password, hostname = get_db_creds()
    min_rating = 10.0
    lowest_rated_movies =[]
    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
    except Exception as exp:
        print(exp)
        import MySQLdb
        cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)

    cur = cnx.cursor()

    cur.execute("SELECT title, year, actor, director, rating FROM movies2")
    results = cur.fetchall()
    # first get max rating 
    for title, year, actor, director, rating in results:
        if min_rating > float(rating):
            min_rating = float(rating)

    # now grab all movies with highest rating
    for title, year, actor, director, rating in results:
        if min_rating == float(rating):
            movie = ['%s, %s, %s, %s, %s' % (title, year, actor, director, float(rating))]
            lowest_rated_movies.append(movie)

    return render_template('index.html', messages=lowest_rated_movies)

@app.route("/")
def hello():
    print("Printing available environment variables")
    print(os.environ)
    print("Before displaying index.html")
    entries = query_data()
    print("Entries: %s" % entries)
    return render_template('index.html', entries=entries)


if __name__ == "__main__":
    app.debug = True
    app.run(host='0.0.0.0')

